# https://cran.r-project.org/web/packages/censusapi/vignettes/getting-started.html

# install.packages("openssl")
# install.packages("httr")
# install.packages("censusapi")


## 1: GET METADATA ##
library(censusapi)

Sys.setenv(CENSUS_KEY="8c9d0df9dbcccd2f3aea980538673ea99d4ca365") # add key to .Renviron
readRenviron(".Renviron") # Reload .Renviron
Sys.getenv("CENSUS_KEY") # Check it expected key is output on console


# df.APIs <- listCensusApis()

misc.year <- 2016
misc.DatasetName <- "acs/acs5"
misc.DatasetName2 <- "acs/acs5/cprofile"
misc.DatasetName3 <- "acs/acs5/profile"
misc.DatasetName4 <- "acs/acs5/subject"

df.2016.ACS.5yr.detailed <- listCensusMetadata(
  name = misc.DatasetName,
  type = "variables",
  vintage = misc.year
)
df.2016.ACS.5yr.cprofile <- listCensusMetadata(
  name = misc.DatasetName2,
  type = "variables",
  vintage = misc.year
)
df.2016.ACS.5yr.profile <- listCensusMetadata(
  name = misc.DatasetName3,
  type = "variables",
  vintage = misc.year
)
df.2016.ACS.5yr.subject <- listCensusMetadata(
  name = misc.DatasetName4,
  type = "variables",
  vintage = misc.year
)


## 2: SPECIFY VARIABLES FOR GEO LEVELS
misc.geoLvls <- c(
  Counties="county:*",
  ZCTA5="zip code tabulation area:*",
  Tracts="tract:*"
)
misc.geoLvls.Keys <- names(misc.geoLvls)
misc.geoLvls.Vals <- misc.geoLvls
misc.year.TargetVars <- c("B01003_001E","B01003_001M")
misc.chosenAPI <- misc.DatasetName
for(i in 1:length(misc.geoLvls)){
  if(misc.geoLvls.Keys[i]=="Tracts"){
    geos <- NULL
    for(f in fips){
      f_fixed <- if(f<10) paste0('0',f) else f
      stateget <- paste0("state:",f_fixed)
      temp <- getCensus(
        name = misc.chosenAPI,
        vars = misc.year.TargetVars,
        region = misc.geoLvls.Vals[i],
        regionin = stateget,
        vintage = misc.year,
        key = Sys.getenv("CENSUS_KEY")
      )
      geos <- rbind(geos,temp)
    }
    assign(
      x = paste0("df.",misc.geoLvls.Keys[i]),
      value = geos
    )
  } else {
    assign(
      x = paste0("df.",misc.geoLvls.Keys[i]),
      value = getCensus(
        name = misc.chosenAPI,
        vars = misc.year.TargetVars,
        region = misc.geoLvls.Vals[[i]],
        vintage = misc.year,
        key = Sys.getenv("CENSUS_KEY")
      )
    )
    if(misc.geoLvls.Keys[i]=="ZCTA5"){
      colnames(get(paste0("df.",misc.geoLvls.Keys[i])))[1] <- "zcta5"
    }
  }
}


## 4: FORMAT COLUMNS
for(df in c("df.Counties","df.ZCTA5","df.Tracts")){
  tempDF <- get(df)
  tempDF[is.na(tempDF)] <- 0
 
  tempDF$pop_formatted_2016 <- formatC(tempDF$B01003_001E, format="d", big.mark=",")

  assign(df,tempDF)
}


## 5: WRITE RESULTS TO CSV
outputDir <- "output"
fileDescription <- "2016-ACS-population"
filenameWithDir <- paste0(outputDir,'/',fileDescription)
write.csv(
  x = df.Counties,
  file = paste0(filenameWithDir,"-counties",".csv"),
  row.names = F
)

write.csv(
  x = df.ZCTA5,
  file = paste0(filenameWithDir,"-zcta5",".csv"),
  row.names = F
)

write.csv(
  x = df.Tracts,
  file = paste0(filenameWithDir,"-tracts",".csv"),
  row.names = F
)