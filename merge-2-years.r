misc.outputDir <- "output"
misc.year1FilePrefix <- "2010-Census-population-"
misc.year2FilePrefix <- "2016-ACS-population-"
misc.geos <- c("counties","zcta5","tracts")

for(geo in misc.geos){
  fileNameWithDir1 <- paste0(misc.outputDir,'/',misc.year1FilePrefix,geo,".csv")
  fileNameWithDir2 <- paste0(misc.outputDir,'/',misc.year2FilePrefix,geo,".csv")
  
  y1 <- paste0("df.year1.",geo)
  
  colClassesVec <- c(
    "state"="character",
    "county"="character",
    "tract"="character",
    "zip.code.tabulation.area"="character"
  )
  
  assign(
    x = y1,
    value = read.csv(
      file = fileNameWithDir1,
      colClasses = colClassesVec
    )
  )
  
  y2 <- paste0("df.year2.",geo)
  assign(
    x = y2,
    value = read.csv(
      file = fileNameWithDir2,
      colClasses = colClassesVec
    )
  )
  
  mergeCols <- NULL
  if(geo=="counties"){
    mergeCols <- c("state","county")
  }
  if(geo=="zcta5"){
    mergeCols <- c("zip.code.tabulation.area")
  }
  if(geo=="tracts"){
    mergeCols <- c("state","county","tract")
  }
  
  merged <- NULL
  mergeVarName <- paste0("df.merge.",geo)
  assign(
    x = mergeVarName,
    value = merge(
      x = get(y1),
      y = get(y2),
      by = mergeCols
    )
  )
  mergedVar <- get(mergeVarName)
  names(mergedVar)[names(mergedVar)=="zip.code.tabulation.area"] <- "zcta5"
}