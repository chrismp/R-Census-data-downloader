# https://cran.r-project.org/web/packages/censusapi/vignettes/getting-started.html

# install.packages("openssl")
# install.packages("httr")
# install.packages("censusapi")

library(censusapi)

Sys.setenv(CENSUS_KEY="8c9d0df9dbcccd2f3aea980538673ea99d4ca365") # add key to .Renviron
readRenviron(".Renviron") # Reload .Renviron
Sys.getenv("CENSUS_KEY") # Check it expected key is output on console


df.APIs <- listCensusApis()

sahieVars <- listCensusMetadata(
  name = "timeseries/healthins/sahie",
  type = "variables"
)

misc.DatasetName <- "acs/acs5"
misc.DatasetName2 <- "acs/acs5/cprofile"
misc.DatasetName3 <- "acs/acs5/profile"
misc.DatasetName4 <- "acs/acs5/subject"

df.2016.ACS.5yr.detailed <- listCensusMetadata(
  name = misc.DatasetName,
  type = "variables",
  vintage = 2016
)
df.2016.ACS.5yr.cprofile <- listCensusMetadata(
  name = misc.DatasetName2,
  type = "variables",
  vintage = 2016
)
df.2016.ACS.5yr.profile <- listCensusMetadata(
  name = misc.DatasetName3,
  type = "variables",
  vintage = 2016
)
df.2016.ACS.5yr.subject <- listCensusMetadata(
  name = misc.DatasetName4,
  type = "variables",
  vintage = 2016
)



## Get specific variables for geo levels
chosenDataset <- misc.DatasetName4
varName <- "S1501_C01_006"
targetVars <- c(
  paste0("S1501_C01_006",'E'),
  paste0("S1501_C01_006",'M'),
  paste0("S1501_C01_012",'E'),
  paste0("S1501_C01_012",'M'),
  paste0("S1501_C01_013",'E'),
  paste0("S1501_C01_013",'M')
) # Change this to get the desired datasets
outputDir <- "output"
filenameWithDir <- paste0(outputDir,'/',"S1501","-college_educated")

zcta5 <- getCensus(
  name = chosenDataset,
  vars = targetVars,
  region = "zip code tabulation area:*",
  vintage = 2016,
  key = Sys.getenv("CENSUS_KEY")
)
colnames(zcta5)[1] <- "zcta5"
prZipCodeStart <- '00'
zcta5 <- zcta5[!(startsWith(zcta5$zcta5,prZipCodeStart)),]

counties <- getCensus(
  name = chosenDataset,
  vars = targetVars,
  region = "county:*",
  vintage = 2016,
  key = Sys.getenv("CENSUS_KEY")
)

tracts <- NULL
for(f in fips){
  f_fixed <- if(f<10) paste0('0',f) else f
  stateget <- paste0("state:",f_fixed)
  temp <- getCensus(
    name = chosenDataset,
    vars = targetVars,
    region = "tract:*",
    regionin = stateget,
    vintage = 2016,
    key = Sys.getenv("CENSUS_KEY")
  )
  tracts <- rbind(tracts,temp)
}

# Format some columns
for(df in c("counties","zcta5","tracts")){
  tempDF <- get(df)
  tempDF[is.na(tempDF)] <- 0
  
  tempDF$pop_25_older_formatted <- formatC(tempDF$S1501_C01_006E, format="d", big.mark=",")
  tempDF$bach_higher <- tempDF$S1501_C01_012E + tempDF$S1501_C01_013E
  tempDF$bach_higher_formatted <- formatC(tempDF$bach_higher, format="d", big.mark=",")
  tempDF$percent_bach_higher <- tempDF$S1501_C01_013E / tempDF$S1501_C01_006E * 100
  tempDF$percent_bach_higher_formatted <- paste0(round(x = tempDF$percent_bach_higher, digits = 1),'%')
  
  tempDF[is.na(tempDF)] <- 0
  assign(df,tempDF)
}

# Write dataframes to CSVs
write.csv(
  x = zcta5,
  file = paste0(filenameWithDir,"-zcta5",".csv"),
  row.names = F
)

write.csv(
  x = counties,
  file = paste0(filenameWithDir,"-counties",".csv"),
  row.names = F
)

write.csv(
  x = tracts,
  file = paste0(filenameWithDir,"-tracts",".csv"),
  row.names = F
)