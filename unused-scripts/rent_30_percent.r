# https://cran.r-project.org/web/packages/censusapi/vignettes/getting-started.html

# install.packages("openssl")
# install.packages("httr")
# install.packages("censusapi")


## 1: GET METADATA ##
library(censusapi)

Sys.setenv(CENSUS_KEY="8c9d0df9dbcccd2f3aea980538673ea99d4ca365") # add key to .Renviron
readRenviron(".Renviron") # Reload .Renviron
Sys.getenv("CENSUS_KEY") # Check it expected key is output on console


# df.APIs <- listCensusApis()

misc.DatasetName <- "acs/acs5"
misc.DatasetName2 <- "acs/acs5/cprofile"
misc.DatasetName3 <- "acs/acs5/profile"
misc.DatasetName4 <- "acs/acs5/subject"

df.2016.ACS.5yr.detailed <- listCensusMetadata(
  name = misc.DatasetName,
  type = "variables",
  vintage = 2016
)
df.2016.ACS.5yr.cprofile <- listCensusMetadata(
  name = misc.DatasetName2,
  type = "variables",
  vintage = 2016
)
df.2016.ACS.5yr.profile <- listCensusMetadata(
  name = misc.DatasetName3,
  type = "variables",
  vintage = 2016
)
df.2016.ACS.5yr.subject <- listCensusMetadata(
  name = misc.DatasetName4,
  type = "variables",
  vintage = 2016
)



## 2: SPECIFY VARIABLES FOR GEO LEVELS
chosenDataset <- misc.DatasetName4
varGroup <- "S0501_C03_001"
targetVars <- c(
  "S0501_C01_137E",
  "S0501_C01_137M",
  "S0501_C01_135E",
  "S0501_C01_135M"
) # Change this to get the desired datasets
outputDir <- "output"
fileNameSuffix <- "-renter_30_percent"
filenameWithDir <- paste0(outputDir,'/',varGroup,fileNameSuffix)
vintageYr <- 2016

counties <- getCensus(
  name = chosenDataset,
  vars = targetVars,
  region = "county:*",
  vintage = vintageYr,
  key = Sys.getenv("CENSUS_KEY")
)


zcta5 <- getCensus(
  name = chosenDataset,
  vars = targetVars,
  region = "zip code tabulation area:*",
  vintage = vintageYr,
  key = Sys.getenv("CENSUS_KEY")
)
colnames(zcta5)[1] <- "zcta5"

tracts <- NULL
for(f in fips){
  f_fixed <- if(f<10) paste0('0',f) else f
  stateget <- paste0("state:",f_fixed)
  temp <- getCensus(
    name = chosenDataset,
    vars = targetVars,
    region = "tract:*",
    regionin = stateget,
    vintage = vintageYr,
    key = Sys.getenv("CENSUS_KEY")
  )
  tracts <- rbind(tracts,temp)
}


## 4: FORMAT COLUMNS
for(df in c("counties","zcta5","tracts")){
  tempDF <- get(df)
  tempDF[is.na(tempDF)] <- 0
  
  tempDF$population_formatted <- formatC(tempDF$S0101_C01_001E, format="d", big.mark=",")
  tempDF$foreign_born_population_formatted <- formatC(tempDF$S0501_C03_001E,format='d',big.mark=',')
  tempDF$foreign_born_population_percent <- tempDF$S0501_C03_001E / tempDF$S0501_C03_001E * 100
  tempDF[is.nan(tempDF)] <- 0
  tempDF$foreign_born_population_percent_formatted <- paste0(round(tempDF$foreign_born_population_percent,1),'%')

  assign(df,tempDF)
}


## 5: WRITE RESULTS TO CSV
write.csv(
  x = zcta5,
  file = paste0(filenameWithDir,"-zcta5",".csv"),
  row.names = F
)

write.csv(
  x = counties,
  file = paste0(filenameWithDir,"-counties",".csv"),
  row.names = F
)

write.csv(
  x = tracts,
  file = paste0(filenameWithDir,"-tracts",".csv"),
  row.names = F
)