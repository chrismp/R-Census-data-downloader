# https://cran.r-project.org/web/packages/censusapi/vignettes/getting-started.html

# install.packages("openssl")
# install.packages("httr")
# install.packages("censusapi")


## 1: GET METADATA ##
library(censusapi)

Sys.setenv(CENSUS_KEY="8c9d0df9dbcccd2f3aea980538673ea99d4ca365") # add key to .Renviron
readRenviron(".Renviron") # Reload .Renviron
Sys.getenv("CENSUS_KEY") # Check it expected key is output on console


# df.APIs <- listCensusApis()

misc.year <- 2010
misc.APIName <- "sf1"
df.2010 <- listCensusMetadata(
  name = misc.APIName,
  type = "variables",
  vintage = misc.year
)

## 2: SPECIFY VARIABLES FOR GEO LEVELS
misc.geoLvls <- c(
  Counties="county:*",
  ZCTA5="zip code tabulation area:*",
  Tracts="tract:*"
)
misc.geoLvls.Keys <- names(misc.geoLvls)
misc.geoLvls.Vals <- misc.geoLvls
misc.year.TargetVars <- c("P0010001")
for(i in 1:length(misc.geoLvls)){
  if(misc.geoLvls.Keys[i]=="Tracts" || misc.geoLvls.Keys[i]=="ZCTA5"){
    geos <- NULL
    for(f in fips){
      f_fixed <- if(f<10) paste0('0',f) else f
      stateget <- paste0("state:",f_fixed)
      temp <- getCensus(
        name = misc.APIName,
        vars = misc.year.TargetVars,
        region = misc.geoLvls.Vals[i],
        regionin = stateget,
        vintage = misc.year,
        key = Sys.getenv("CENSUS_KEY")
      )
      geos <- rbind(geos,temp)
    }
    assign(
      x = paste0("df.",misc.geoLvls.Keys[i]),
      value = geos
    )
    # if(misc.geoLvls.Keys[i]=="ZCTA5"){
    #   colnames(get(paste0("df.",misc.geoLvls.Keys[i])))[1] <- "zcta5"
    # }
  } else {
    assign(
      x = paste0("df.",misc.geoLvls.Keys[i]),
      value = getCensus(
        name = misc.APIName,
        vars = misc.year.TargetVars,
        region = misc.geoLvls.Vals[[i]],
        vintage = misc.year,
        key = Sys.getenv("CENSUS_KEY")
      )
    )
  }
}


## 4: FORMAT COLUMNS
for(df in c("df.Counties","df.ZCTA5","df.Tracts")){
  tempDF <- get(df)
  tempDF[is.na(tempDF)] <- 0
 
  if(df=="df.ZCTA5"){
    tempDF <- tempDF[ , !(names(tempDF) %in% c("state"))]
  }
  
  tempDF$pop_formatted_2010 <- formatC(tempDF$P0010001, format="d", big.mark=",")

  assign(df,tempDF)
}


## 5: WRITE RESULTS TO CSV
outputDir <- "output"
fileDescription <- "2010-Census-population"
filenameWithDir <- paste0(outputDir,'/',fileDescription)
write.csv(
  x = df.Counties,
  file = paste0(filenameWithDir,"-counties",".csv"),
  row.names = F
)

write.csv(
  x = df.ZCTA5,
  file = paste0(filenameWithDir,"-zcta5",".csv"),
  row.names = F
)

write.csv(
  x = df.Tracts,
  file = paste0(filenameWithDir,"-tracts",".csv"),
  row.names = F
)